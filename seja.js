if (!process.env.PORT)
  process.env.PORT = 8080;
//če odpremo nov tab, smatra kot isto sejo in se ne pobriše iz košarice
// Priprava strežnika
var express = require('express');
var expressSession = require('express-session');  //omogoča podporo sejam. Ali imamo id v URL-ju, ali pa kot cookie.
                                                  //Seja je globalna spremenljivka na ravni komunikacije host-server

var streznik = express();     //strežniku povemo, da bomo uporabljali seje.
streznik.use(
  expressSession({
    secret: '1234567890QWERTY', // Skrivni ključ za podpisovanje piškotkov, da ne more pridt do vdora
    saveUninitialized: true,    // Novo sejo shranimo
    resave: false,              // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 2000              // Seja poteče po 2s neaktivnosti
    }
  })
);

var stDostopov = 0;   //glob spremenljivka, števec vseh dostopov na stran

streznik.get('/', function(zahteva, odgovor) {
  stDostopov++;     //sledi: isto spremenljivko definiramo znotraj seje, in imamo števec refrešov znotraj seje.
  if (zahteva.session.stDostopov) {
    zahteva.session.stDostopov++;
  } else {
    zahteva.session.stDostopov = 1;
  }
  //izpis uporabniku
  odgovor.send('<h1>Globalne spremenljivke vs. seja</h1>' +
    '<p style="font-size:150%">' + 'To je že <strong>' +
    stDostopov + 'x</strong> dostop do strežnika, ' +
    'medtem ko ste v tej seji dostopali <strong>' +
    zahteva.session.stDostopov + 'x</strong>.' +
    '</p>');
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik pognan!");
})
